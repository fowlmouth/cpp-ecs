
#pragma once

#include <unordered_map>
#include <functional>
#include <algorithm>
#include <vector>
#include <memory>

namespace fowl
{

template
<
	typename EventID_,
	typename EventData_,
	typename EventResult_
>
struct EventDispatcher
{
	using Self = EventDispatcher< EventID_, EventData_, EventResult_ >;
	using EventID = EventID_;
	using EventData = EventData_;
	using EventResult = EventResult_;
	using Callback = std::function< bool(const EventID&, const EventData&, EventResult&) >;

	struct EventSubscriber
	{
		std::unordered_map< EventID, std::vector< Callback > > handlers;

		EventSubscriber()
		{}
		EventSubscriber(EventSubscriber&& subscriber)
		: handlers(std::move(subscriber.handlers))
		{}

		inline void subscribe(const EventID& eventID, const Callback& callback)
		{
			handlers[eventID].push_back(callback);
		}

		inline bool emit(const EventID& eventID, const EventData& eventData, EventResult& result)
		{
			const auto iter = handlers.find(eventID);
			if(iter != handlers.end())
				for(const auto handler : iter->second)
					if(handler(eventID, eventData, result))
						return true;
			return false;
		}
	};

	using Subscriber = std::shared_ptr< EventSubscriber >;
	std::vector< Subscriber > subs;

	EventDispatcher()
	{}
	EventDispatcher(Self&& source)
	: subs(std::move(source.subs))
	{}

	inline Subscriber new_subscriber()
	{
		Subscriber s = std::make_shared< EventSubscriber >();
		subs.push_back(s);
		return s;
	}

	inline void add(const Subscriber& subscriber)
	{
		const auto iter = std::find(subs.begin(), subs.end(), subscriber);
		if(iter == subs.end())
			subs.insert(iter, subscriber);
	}

	inline void remove(const Subscriber& subscriber)
	{
		const auto iter = std::find(subs.begin(), subs.end(), subscriber);
		if(iter != subs.end())
			subs.erase(iter);
	}

	inline bool emit(const EventID& eventID, const EventData& eventData, EventResult& result)
	{
		for(const auto& sub : subs)
			if(sub->emit(eventID, eventData, result))
				return true;
		return false;
	}
};

}

