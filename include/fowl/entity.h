#pragma once

#include "typeset.h"
#include "event.h"

#include <memory>
#include <bitset>

namespace fowl
{
namespace entity
{

using EntityID = std::size_t;
using ComponentID = unsigned int;

template<typename EM>
struct BaseAllocator
{
    using Entity = typename EM::Entity;

    virtual void* alloc (Entity& entity) = 0;
    virtual void dealloc(Entity& entity, void*) = 0;
};

template<typename T, typename EM>
struct AllocateComponent: BaseAllocator<EM>
{
    using BaseAlloc = BaseAllocator<EM>;
    using Entity = typename BaseAlloc::Entity;

    void* alloc (Entity&) { return (void*)new T; }
    void dealloc(Entity&, void* ptr) { delete (T*)ptr; }
};

template<typename T, typename Derived>
struct ComponentBehavior
{
};

template
<
    typename EntityType,
    typename T,
    template<typename,typename>typename ComponentBehavior_// = ComponentBehavior
>
struct ComponentHandle
:
    //public ComponentBehavior< T, ComponentHandle<EntityType,T> >,
    public ComponentBehavior_< T, ComponentHandle< EntityType, T, ComponentBehavior_ > >,
    public EntityType
{
    using Entity = EntityType;
    using type = T;

    using Self = ComponentHandle< EntityType, T, ComponentBehavior_ >;
    // using ComponentBehavior = ComponentBehavior_< T, ComponentHandle< EntityType, T >>;
    
    T* data = nullptr;

    inline ComponentHandle(Entity entity, T* data)
    : Entity(entity), data(data)
    {}

    inline T* operator->() { return data; }
    inline T& operator* () { return *data; }

    inline T* get() { return data; }
    //inline operator Entity() { return entity; }

    inline bool is_valid();
    inline explicit operator bool() { return is_valid(); }
};

template<int MaxComponents>
using ComponentMask = std::bitset<MaxComponents>;


template<typename Set, typename Mask, typename...>
struct set_filters_impl;

template<typename Set, typename Mask, typename Co, typename... Cos>
struct set_filters_impl<Set, Mask, Co, Cos...>
{
    inline static void set(Mask& mask)
    {
        mask.set(Set::template get_id<Co>());
        set_filters_impl<Set, Mask, Cos...>::set(mask);
    }
};

template<typename Set, typename Mask>
struct set_filters_impl<Set, Mask>
{
    inline static void set(Mask& mask)
    {
    }
};

template<typename Set, typename Mask, typename... Cos>
inline static void set_filters(Mask& mask)
{
    set_filters_impl<Set, Mask, Cos...>::set(mask);
}


template<typename CS>
struct E;

template<
    typename CS,
    int MaxComponents_,
    template<typename,typename> typename AllocateComponent_,
    template<typename,typename> typename ComponentBehavior_,
    typename EventDispatcher_
>
struct EM;

template<typename EM>
struct BaseSystem
{
    using EntityManager = EM;
    
    EntityManager* em;
    
    virtual ~BaseSystem()
    {
    }

    virtual void update (float dt)
    {
    }
};

template<
    typename CS,
    int MaxComponents_,
    template<typename,typename> typename AllocateComponent_,
    template<typename,typename> typename ComponentBehavior_,
    typename EventDispatcher_
>
struct EM
{

    using Self = EM< CS, MaxComponents_, AllocateComponent_, ComponentBehavior_, EventDispatcher_ >;
    
    using EntityID = EntityID;
    using Entity = E<Self>;
    using BaseSys = BaseSystem<Self>;
    using ComponentMask = ComponentMask<MaxComponents_>;

    using TypeSet = CS;
    static const int MaxComponents = MaxComponents_;
    template<typename Component>
    using AllocateComponent = AllocateComponent_<Component, Entity>;
    template<typename T, typename Derived>
    using ComponentBehavior = ComponentBehavior_<T,Derived>;

    using EventDispatcher = EventDispatcher_;

    using BaseAllocator = BaseAllocator<Entity>;

    std::vector<BaseAllocator*> allocators;
    std::vector< std::shared_ptr< BaseSys >> systems;
    template<typename T>
    using ComponentHandle = ComponentHandle<Entity, T, ComponentBehavior_ >;

    EventDispatcher event_dispatcher;

    struct EntityData
    {
        bool valid;
        ComponentMask components;
        EntityID self;
        EntityID parent;
        std::vector<EntityID> children;
        std::vector<void*> data;
        typename EventDispatcher_::Subscriber subscriber;

        EntityData(EntityData&& ed)
        :   valid(ed.valid),
            components(std::move(ed.components)),
            self(ed.self),
            parent(ed.parent),
            children(std::move(ed.children)),
            data(std::move(ed.data)),
            subscriber(std::move(ed.subscriber))
        {
        }

        EntityData()
        :   valid(false),
            self(0),
            parent(0),
            subscriber(nullptr)
        {
        }

    };
    std::vector<EntityData> entities;


    template<typename T>
    T* alloc(Entity& entity_handle)
    {
        const auto id = CS::template get_id<T>(); //getComponentID<T>::id;
        if(allocators.size() < id+1)
            allocators.resize(CS::num_types(), nullptr);
        if(!allocators[id])
            allocators[id] = new AllocateComponent<T>;
        return (T*)(allocators[id]->alloc(entity_handle));
    }

    EntityID next_id()
    {
        std::size_t id = 1;
        for(; id < entities.size(); ++id)
        {
            if(!entities[id].valid)
                return id;
        }
        return id;
    }

    Entity create_entity(EntityID parent);
    void destroy_entity(EntityID id);
    void clear_entities();

    ~EM()
    {
        clear_entities();
    }

    EM(std::size_t initial_entity_pool = 512)
    {
        entities.resize(initial_entity_pool);
    }

    Entity get(EntityID entity);

    bool is_valid(EntityID id)
    {
        return id < entities.size() && entities[id].valid;
    }
    bool has_component(EntityID id, ComponentID component)
    {
        return entities[id].components[component];
    }

    template<typename T>
    ComponentHandle<T> create_component(EntityID entity)
    {
        assert(is_valid(entity));
        ComponentID comp = CS::template get_id<T>();//getComponentID<T>::id;

        if(has_component(entity, comp))
        {
            return get_component<T>(entity);
        }
        //assert(! has_component(entity, comp));
        Entity entity_handle{ entity, this };
        T* data = alloc<T>(entity_handle);

        auto& storage = entities[entity];
        if(storage.data.size() < comp+1) storage.data.resize(comp+1, nullptr);
        storage.data[comp] = data;
        storage.components.set(comp, true);

        return ComponentHandle<T>(entity_handle, data);
    }

    template<typename T>
    ComponentHandle<T> get_component(EntityID entity) 
    {
        auto id = TypeSet::template get_id<T>();
        if(entities[entity].data.size() < id+1 ||//getComponentID<T>::id+1 ||
            !entities[entity].data[id])//getComponentID<T>::id])
            return ComponentHandle<T>(Entity{entity, this}, nullptr);
        else
            return ComponentHandle<T>(Entity{entity, this}, (T*)entities[entity].data[id] );
    }

    EntityID parent (EntityID entity)
    {
        return entities[entity].parent;
    }



    std::shared_ptr< BaseSys > add_system(const std::shared_ptr< BaseSys >& system)
    {
        systems.push_back(system);
        system->em = this;
        return system;
    }

    template<typename System, typename... Args>
    std::shared_ptr< System > add_system(Args&&... args)
    {
        std::shared_ptr< System > system = std::make_shared< System >(std::forward<Args>(args)...);
        systems.push_back(system);
        system->em = this;
        return system;
    }

    void update_all (float dt)
    {
        for(auto& sys: systems)
            sys->update(dt);
    }

    template<typename...Cs>
    struct view
    {
        ComponentMask mask;
        Self* em;
        
        view(Self* em, ComponentMask m)
        : mask(m), em(em)
        {
        }

        template
        <
            typename Callback
        >
        void each (const Callback& callback) const
        {
            for(auto& entity: em->entities)
            {
                if(entity.valid && (entity.components & mask) == mask)
                {
                    callback(
                        Entity{entity.self, em},
                        (*em->template get_component<Cs>(entity.self))...
                    );
                }
            }
        }

        template
        <
            typename Callback
        >
        void each (Callback& callback) const
        {
            for(auto& entity : em->entities)
                if(entity.valid && (entity.components & mask) == mask)
                    callback(
                        Entity{entity.self, em},
                        (*em->template get_component<Cs>(entity.self))...
                    );
        }

    };

    template
    <
        typename Callback,
        typename... Cs
    >
    inline void each (const Callback& callback)
    {
        ComponentMask filter;
        set_filters<ComponentMask, Cs...>(filter);
        view<Cs...>(this, filter).each(callback);
    }

    template
    <
        typename Callback,
        typename... Cs
    >
    inline void each (Callback& callback)
    {
        ComponentMask filter;
        set_filters<ComponentMask, Cs...>(filter);
        view<Cs...>(this, filter).each(callback);
    }

    inline void emit (
            const typename EventDispatcher::EventID& event_id,
            const typename EventDispatcher::EventData& event_data,
            typename EventDispatcher::EventResult& event_result)
    {
        event_dispatcher.emit(event_id, event_data, event_result);
    }

};


template<typename EM>
struct E
{
    using EntityManager = EM;

    using Entity = E<EM>;
    // using EntityManager = EM<CS>;
    using ComponentMask = typename EntityManager::ComponentMask;
    template<typename T>
    using Handle = typename EntityManager::template ComponentHandle<T>; //ComponentHandle<Entity, T>;
    using TypeSet = typename EntityManager::TypeSet; // CS;

    using ID = EntityID;

    using EventDispatcher = typename EM::EventDispatcher;

    EntityID id = 0;
    EntityManager* m = nullptr;


    Entity create_child()
    {
        return m->create_entity(id);
    }

    explicit operator EntityID() { return id; }
    inline bool is_valid() { return m->is_valid(id); }

    template<typename T>
    Handle<T> create_component()
    {
        return m->template create_component<T>(id);
    }

    template<typename T>
    Handle<T> create()
    {
        return m->template create_component<T>(id);
    }

    template<typename T>
    Handle<T> get_component()
    {
        return m->template get_component<T>(id);
    }

    template<typename T>
    Handle<T> get()
    {
        return m->template get_component<T>(id);
    }

    Entity create_child() const
    {
        return m->create_entity(id);
    }

    // search parents for the first entity containing component T
    template<typename T>
    Handle<T> search_parents() const
    {
        EntityID e = id;
        while(e)
        {
            if(!e || !m->is_valid(e))
                break;

            auto handle = m->template get_component<T>(e);
            if(handle.is_valid())
                return handle;

            e = m->parent(e);
        }
        return Handle<T>(Entity{0, m}, (T*)0);
    }

    const typename EntityManager::EntityData& get_data() const
    {
        return m->entities[id];
    }


    template
    <
        typename Callback,
        typename... Cs
    >
    inline void each_child (const Callback& callback) const
    {
        ComponentMask filter;
        set_filters<ComponentMask, Cs...>(filter);
        auto& data = get_data();
        for(auto id : data.children)
        {
            auto child = m->get(id);
            if(child.is_valid() && (child.get_data().components & filter) == filter)
                callback(child, (*child.template get_component<Cs>())...);
        }
    }

    bool subscribe(const typename EventDispatcher::EventID& event_id, const typename EventDispatcher::Callback& cb)
    {
        if(!is_valid())
            return false;

        auto& data = m->entities[id];
        if(! data.subscriber)
            data.subscriber = m->event_dispatcher.new_subscriber();
        data.subscriber->subscribe(event_id, cb);
        return true;
    }

};


template
<
    typename E,
    typename T,
    template<typename,typename>typename Behavior_
>
bool ComponentHandle<E,T,Behavior_>::is_valid ()
{
    return E::is_valid() && (bool)data;
}

template
<
    typename CS,
    int MaxComponents_,
    template<typename,typename>typename Alloc_,
    template<typename,typename>typename Behavior_,
    typename EventDispatcher_
>
typename EM<CS, MaxComponents_, Alloc_, Behavior_, EventDispatcher_>::Entity
EM<CS, MaxComponents_, Alloc_, Behavior_, EventDispatcher_>::create_entity(EntityID parent)
{
    EntityID id = next_id();
    if(entities.size() < id+1) entities.resize(id+1);
    if(parent) entities[parent].children.push_back(id);
    EntityData& d = entities[id];
    d.valid = true;
    d.self  = id;
    d.parent = parent;
    return Entity{id, this};
}

template
<
    typename CS,
    int MaxComponents_,
    template<typename,typename>typename Alloc_,
    template<typename,typename>typename Behavior_,
    typename EventDispatcher_
>
void
EM<CS, MaxComponents_, Alloc_, Behavior_, EventDispatcher_>::destroy_entity(EntityID id)
{
    if(! entities[id].valid) return;
    auto& d = entities[id];
    d.valid = false;
    if(d.parent)
    {
        auto& p = entities[d.parent ];
        //std::erase(std::remove(p.children.begin(), p.children.end(), id));
        const auto iter = std::find(p.children.begin(), p.children.end(), id);
        if(iter != p.children.end()) p.children.erase(iter);
    }
    for(auto c: d.children)
    {
        entities[c].parent = d.parent;
        if(d.parent) entities[d.parent].children.push_back(c);
    }

    Entity entity_handle{ id, this };
    d.parent = 0;
    for(int i = 0; i < d.data.size(); ++i)
        if(d.data[i])
        {
            allocators[i]->dealloc(entity_handle, d.data[i]);
            d.data[i] = nullptr;
        }
    d.components.reset(false);

    return;
}

template
<
    typename CS,
    int MaxComponents_,
    template<typename,typename>typename Alloc_,
    template<typename,typename>typename Behavior_,
    typename EventDispatcher_
>
void
EM<CS, MaxComponents_, Alloc_, Behavior_, EventDispatcher_>::clear_entities()
{
    for(std::size_t id = 0; id < entities.size(); ++id)
    {
        const auto& data = entities.at(id);
        if(data.valid)
        {
            destroy_entity(id);
        }
    }
}


template
<
    typename CS,
    int MaxComponents_,
    template<typename,typename>typename Alloc_,
    template<typename,typename>typename Behavior_,
    typename EventDispatcher_
>
typename EM<CS, MaxComponents_, Alloc_, Behavior_, EventDispatcher_>::Entity
EM<CS, MaxComponents_, Alloc_, Behavior_, EventDispatcher_>::get (EntityID entity)
{
    return {is_valid(entity) ? entity : 0, this};
}

template<typename EM, typename... Cs>
struct System: public BaseSystem<EM>
{
    using EntityManager = typename BaseSystem<EM>::EntityManager;
    using ComponentMask = typename EntityManager::ComponentMask;
    using Entity = typename EntityManager::Entity;
    using EventDispatcher = typename EntityManager::EventDispatcher;

    ComponentMask filter;
    typename EntityManager::EventDispatcher::Subscriber subscriber;

    System()
    {
        set_filters<typename EntityManager::TypeSet, ComponentMask, Cs...>(filter);
    }

    typename EntityManager::template view<Cs...>
    entities()
    {
        return typename EntityManager::template view<Cs...>(this->em, filter);
    }

    template<typename Callback>
    inline void each (const Callback& callback)
    {
        entities().template each<Callback>(callback);
    }

    template<typename Callback>
    inline void each (Callback& callback)
    {
        entities().template each<Callback>(callback);
    }

    template< typename Callback >
    inline void subscribe(
            const typename EventDispatcher::EventID& event_id,
            const Callback& callback)
    {
        if(! subscriber)
            subscriber = this->em->event_dispatcher.new_subscriber();

        subscriber->subscribe(event_id, callback);
    }


};

template
<
    typename Tag,
    int MaxComponents_ = 32,
    template<typename,typename>typename Alloc_ = AllocateComponent,
    template<typename,typename>typename ComponentBehavior_ = ComponentBehavior,
    typename EventDispatcher_ = fowl::EventDispatcher< int, int, int >
>
struct Domain
{
    using TypeSet = fowl::TypeSet< Tag >;
    
    using EntityManager = entity::EM<TypeSet, MaxComponents_, Alloc_, ComponentBehavior_, EventDispatcher_>;
    
    static const int MaxComponents = EntityManager::MaxComponents;
    
    using Entity = typename EntityManager::Entity; // entity::E<Set>;
    using EntityID = typename Entity::ID;

    template<typename Component>
    using AllocateComponent = Alloc_<Component, Entity>;
    template<typename Component, typename Derived>
    using ComponentBehavior = ComponentBehavior_<Component, Derived>;

    using EventDispatcher = typename EntityManager::EventDispatcher;

    template<typename Component>
    using ComponentHandle = typename EntityManager::template ComponentHandle< Component >;
    
    template<typename... Cx>
    using System = entity::System<EntityManager, Cx...>;

    template< typename Tag_ >
    using WithTag = Domain< Tag_, MaxComponents_, Alloc_, ComponentBehavior_, EventDispatcher_ >;

    template< int MaxComponents__ >
    using WithMaxComponents = Domain< Tag, MaxComponents__, Alloc_, ComponentBehavior_, EventDispatcher_ >;

    template< template<typename,typename>typename AllocateComponent__ >
    using WithAllocateComponent = Domain< Tag, MaxComponents_, AllocateComponent__, ComponentBehavior_, EventDispatcher_ >;

    template< template<typename,typename>typename ComponentBehavior__ >
    using WithComponentBehavior = Domain< Tag, MaxComponents_, Alloc_, ComponentBehavior__, EventDispatcher_ >;

    template< typename EventDispatcher__ >
    using WithEventDispatcher = Domain< Tag, MaxComponents_, Alloc_, ComponentBehavior_, EventDispatcher__ >;

};


} //entity::
} //fowl::

