// clang++ -std=c++1z test-destruction.cpp -o test-destruction

#include "../include/fowl/entity.h"

#include <iostream>


struct Position
{
	float x,y;
};
std::ostream& operator<< (std::ostream& lhs, Position& rhs)
{
	return lhs << '(' << rhs.x << ", " << rhs.y << ')';
}

struct Velocity
{
	float x,y;
};

std::ostream& operator<< (std::ostream& lhs, Velocity& rhs)
{
	return lhs << '(' << rhs.x << ", " << rhs.y << ')';
}

template<typename Component, typename Entity>
struct MyAllocator
: fowl::entity::AllocateComponent< Component, Entity >
{
};

template<typename Entity>
struct MyAllocator< Position, Entity >
: fowl::entity::AllocateComponent< Position, Entity >
{
	void dealloc(Entity& entity, void* position)
	{
		std::cout << "custom deallocate Position for entity " << entity.id << std::endl;
		delete static_cast<Position*>(position);
	}

	void* alloc(Entity& entity)
	{
		std::cout << "custom allocate Position for entity " << entity.id << std::endl;
		return static_cast<void*>(new Position);
	}
};

struct t1;
using domain = fowl::entity::Domain< t1 >
	::WithAllocateComponent< MyAllocator >;

int main()
{
	domain::EntityManager em;

	auto ent = em.create_entity(0);

	auto pos = ent.create< Position >();// em.create_component<Position>(ent);
	auto vel = ent.create< Velocity >();// em.create_component<Velocity>(ent);

	// lets test the EM destructor
	// em.destroy_entity(ent.id);

	return 0;
}
