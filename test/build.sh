#!/bin/bash

build() {
	FILE=$1
	echo "** Building $FILE"
	clang++ -std=c++1z -o $FILE $FILE.cpp
}

build test-events
build test-destructor
build test-entity-id
build test
