// clang++ -std=c++1z test-destruction.cpp -o test-destruction

#include "../include/fowl/entity.h"

#include <iostream>
#include <variant>

enum Event
{
	Event1, Event2
};
using EventData = std::variant< int, std::string >;

struct t1;
using domain = fowl::entity::Domain< t1 >
	::WithEventDispatcher< fowl::EventDispatcher< Event, EventData, int >>;

struct TestSystem : domain::System< >
{

};

int main()
{
	domain::EntityManager em;

	auto sys = em.add_system< TestSystem >();
	sys->subscribe(Event::Event1,
		[](const domain::EventDispatcher::EventID& event_id,
			const domain::EventDispatcher::EventData& event_data,
			int& result)
		{
			std::cout << "from a system: " << std::get< std::string >(event_data) << std::endl;
			return false;
		}
	);

	auto ent = em.create_entity(0);

	ent.subscribe(Event::Event1,
		[](const domain::EventDispatcher::EventID& event_id,
			const EventData event_data,
			int& result)
		{
			std::cout << "from an entity: " << std::get< std::string >(event_data) << std::endl;
			return false;
		}
	);

	int unused;
	em.emit(Event::Event1, "hello", unused);

	return 0;
}
