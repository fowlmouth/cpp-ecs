// clang++ -std=c++1z test.cpp -o test

#include "../include/fowl/entity.h"

#include <iostream>


struct Position
{
	float x,y;
};
std::ostream& operator<< (std::ostream& lhs, Position& rhs)
{
	return lhs << '(' << rhs.x << ", " << rhs.y << ')';
}

struct Velocity
{
	float x,y;
};

std::ostream& operator<< (std::ostream& lhs, Velocity& rhs)
{
	return lhs << '(' << rhs.x << ", " << rhs.y << ')';
}

struct t1;
using domain = fowl::entity::Domain< t1 >;

struct PhysicsSystem : domain::System< Position, Velocity >
{
	void update(float dt)
	{
		each([&](const Entity& entity, Position& pos, Velocity& vel){
			pos.x += vel.x * dt;
			pos.y += vel.y * dt;
		});
	}
};

struct StatefulCallback
{
	std::vector< domain::Entity::ID > ids;

	template<typename... Cs>
	void operator() (const domain::Entity& entity, Cs& ...arg)
	{
		ids.push_back(entity.id);
	}
};

int main()
{
	domain::EntityManager em;

	auto physics_system = em.add_system< PhysicsSystem >();

	auto ent = em.create_entity(0);

	auto pos = ent.create< Position >();// em.create_component<Position>(ent);
	auto vel = ent.create< Velocity >();// em.create_component<Velocity>(ent);

	std::cout << "Pos: " << *pos << std::endl
		<< "Vel: " << *vel << std::endl;

	vel->x = 1.0;
	vel->y = 0.5;


	int iterations = 10;
	std::cout << "New Vel: " << *vel << std::endl
		<< "Iterating " << iterations << " times" << std::endl;

	for(int i = 0; i < 10; ++i)
	{
		em.update_all(1.0);
		std::cout << "  Pos: " << *ent.get< Position >() << std::endl;
	}

	StatefulCallback collectIDs;
	physics_system->each(collectIDs);
	for(auto id : collectIDs.ids)
	{
		std::cout << id << ' ';
	}
	std::cout << std::endl;

	return 0;
}
