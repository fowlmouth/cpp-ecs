// clang++ -std=c++1z test-entity-id.cpp -o test-entity-id

#include "../include/fowl/entity.h"

#include <iostream>
#include <cassert>

struct t1;
using domain = fowl::entity::Domain< t1 >;

int main()
{
	domain::EntityManager em;

	auto ent = em.create_entity(0);
	auto old_ent_id = ent.id;
	em.destroy_entity(ent.id);

	ent = em.create_entity(0);
	assert( ent.id == old_ent_id );

	return 0;
}
