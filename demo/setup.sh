#!/bin/sh

# install llvm8, sfml 2.5.1 and openal-soft
brew install llvm sfml openal-soft

# clone json11 
git clone git@github.com:dropbox/json11.git

