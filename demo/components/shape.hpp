#pragma once

#include <SFML/Graphics.hpp>

namespace SimpleGame {
	namespace Components {

		struct Shape
		{
			enum{ Circle, Poly } shape;
			float radius;
			sf::Vector2f origin;
			std::vector< sf::Vector2f > points;

			Shape()
			: shape(Circle), radius(0)
			{
			}
		};

		std::ostream& operator<< (std::ostream& lhs, const Shape& rhs)
		{
			if(rhs.shape == Shape::Circle)
				return lhs << "circle(radius=" << rhs.radius << ')';

			return lhs;
		}

	} // Components

	namespace Data {

		template<>
		struct Loader< json11::Json, Components::Shape >
		{
			static bool load(const json11::Json& data, Components::Shape& shape)
			{
				std::string shape_type = data["shape"].string_value();
				if(shape_type == "circle")
				{
					shape.shape = Components::Shape::Circle;
					shape.radius = data["radius"].number_value();
					if(data["origin"].is_null())
						shape.origin = sf::Vector2f(shape.radius/2, shape.radius/2);
					else
						shape.origin = sf::Vector2f(
							data["origin"][0].number_value(),
							data["origin"][1].number_value());

					return true;
				}
				else if(shape_type == "poly")
				{
					shape.shape = Components::Shape::Poly;

					sf::Vector2f min, max;
					for(auto& point : data["points"].array_items())
					{
						float x = point[0].number_value();
						float y = point[1].number_value();
						min = sf::Vector2f(std::min(min.x, x), std::min(min.y, y));
						max = sf::Vector2f(std::max(max.x, x), std::max(max.y, y));
						shape.points.push_back(
							sf::Vector2f(
								point[0].number_value(),
								point[1].number_value()
							)
						);
					}

					if(data["origin"].is_null())
						shape.origin = sf::Vector2f(
							((max.x - min.x) / 2) + min.x,
							((max.y - min.y) / 2) + min.y
						);
					else
						shape.origin = sf::Vector2f(
							data["origin"][0].number_value(),
							data["origin"][1].number_value()
						);

					return true;
				}
				return false;
			}
		};

	} // Data
} // SimpleGame
