#pragma once

#include "../loader.hpp"

namespace SimpleGame {
	namespace Components {

		struct Position
		{
			float x,y;
			
			Position()
			: x(0), y(0)
			{
			}
		};

		std::ostream& operator<< (std::ostream& lhs, Position& rhs)
		{
			return lhs << '(' << rhs.x << ", " << rhs.y << ')';
		}

	} // Components

	namespace Data {

		template<>
		struct Loader< json11::Json, Components::Position >
		{
			static bool load(const json11::Json& data, Components::Position& position)
			{
				position.x = data["x"].number_value();
				position.y = data["y"].number_value();
				return true;
			}
		};

		template<>
		struct Loader< Components::Position, json11::Json >
		{
			static bool load(const Components::Position& position, json11::Json& data)
			{
				data = json11::Json::object{
					{"x", position.x},
					{"y", position.y}
				};
				return true;
			}
		};

	} // Data
} // SimpleGame
