#pragma once

#include <SFML/Graphics.hpp>

namespace SimpleGame {
	namespace Components {

		struct Color : sf::Color
		{
		};

		// std::ostream& operator<< (std::ostream& lhs, const Shape& rhs)
		// {
		// 	if(rhs.shape == Shape::Circle)
		// 		return lhs << "circle(radius=" << rhs.radius << ')';

		// 	return lhs;
		// }

	} // Components

	namespace Data {

		template<>
		struct Loader< json11::Json, Components::Color >
		{
			static bool load(const json11::Json& data, Components::Color& color)
			{
				if(data["r"].is_number())
					color.r = data["r"].int_value();
				if(data["g"].is_number())
					color.g = data["g"].int_value();
				if(data["b"].is_number())
					color.b = data["b"].int_value();
				if(data["a"].is_number())
					color.a = data["a"].int_value();
				return true;
			}
		};

		template<>
		struct Loader< Components::Color, json11::Json >
		{
			static bool load(const Components::Color& color, json11::Json& data)
			{
				data = json11::Json::object{
					{"r", color.r},
					{"g", color.g},
					{"b", color.b},
					{"a", color.a}
				};
				return true;
			}
		};

	} // Data
} // SimpleGame
