#pragma once

namespace SimpleGame {
	namespace Components {

		struct Velocity
		{
			float x,y;
			
			Velocity()
			: x(0), y(0)
			{
			}
		};

		std::ostream& operator<< (std::ostream& lhs, Velocity& rhs)
		{
			return lhs << '(' << rhs.x << ", " << rhs.y << ')';
		}

	} // Components

	namespace Data {

		template<>
		struct Loader< json11::Json, Components::Velocity >
		{
			static bool load(const json11::Json& data, Components::Velocity& velocity)
			{
				velocity.x = data["x"].number_value();
				velocity.y = data["y"].number_value();
				return true;
			}
		};

		template<>
		struct Loader< Components::Velocity, json11::Json >
		{
			static bool load(const Components::Velocity& velocity, json11::Json& data)
			{
				data = json11::Json::object{
					{"x", velocity.x},
					{"y", velocity.y}
				};
				return true;
			}
		};

	} // Data
} // SimpleGame
