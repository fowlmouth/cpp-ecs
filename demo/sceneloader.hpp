#pragma once

#include "json11/json11.hpp"

#include "loader.hpp"
#include "components.hpp"
#include "systems.hpp"

#include <map>
#include <functional>

namespace SimpleGame {
	namespace Data {

		template
		<
			typename DataType,
			typename KeyType
		>
		struct ComponentDataFetch
		{
			static bool fetch(const DataType&, const KeyType&, DataType&)
			{
				return false;
			}
		};

		template<>
		struct ComponentDataFetch< json11::Json, std::string >
		{
			static bool fetch(const json11::Json& input, const std::string& key, json11::Json& output)
			{
				if(!input.is_object())
					return false;

				auto& object_members = input.object_items();
				const auto iter = object_members.find(key);
				if(iter == object_members.end())
					return false;

				output = iter->second;
				return true;
			}
		};


		template
		<
			typename Domain_,
			template<typename, typename> typename Loader_,
			template<typename, typename> typename Fetcher_,
			typename DataType_,
			typename KeyType_
		>
		struct LoaderDispatcher
		{
			using Domain = Domain_;
			using TypeSet = typename Domain::TypeSet;
			template
			<
				typename From,
				typename To
			>
			using Loader = Loader_< From, To >;
			template
			<
				typename DataType,
				typename KeyType
			>
			using Fetcher = Fetcher_< DataType, KeyType >;
			using DataType = DataType_;
			using KeyType = KeyType_;

			using Self = LoaderDispatcher< Domain_, Loader_, Fetcher_, DataType_, KeyType_ >;
			using LoaderWrapper = std::function<
				void(typename Domain::Entity, const void*)
			>;

			std::map< KeyType, LoaderWrapper > loaders;

			template<typename Component>
			Self& add(const KeyType& key)
			{
				loaders[key] = [](typename Domain::Entity entity, const void* data_)
				{
					auto handle = entity.template create_component< Component >();
					const DataType& data = *static_cast<const DataType*>(data_);
					Loader< DataType, Component >::load(data, *handle);
				};
				return *this;
			}

			void load(typename Domain::Entity entity, const DataType& data_set)
			{
				using Fetch = Fetcher< DataType, KeyType >;
				for(auto& load_function : loaders)
				{
					auto& key = load_function.first;
					auto& fn = load_function.second;

					DataType data;
					if(Fetch::fetch(data_set, key, data))
						fn(entity, static_cast<const void*>(&data));
				}
			}
		};


		template
		<
			typename DataType,
			typename KeyType
		>
		struct SystemDataFetch
		{
			static bool fetch(const DataType& data, const KeyType& key, DataType& output)
			{

				return false;
			}
		};

		template<>
		struct SystemDataFetch< json11::Json, std::string >
		{
			static bool fetch(const json11::Json& data, const std::string& key, json11::Json& output)
			{
				if(data.is_object() && data["type"].is_string() && data["type"].string_value() == key)
				{
					output = data;
					return true;
				}
				return false;
			}
		};

		template
		<
			typename Domain_,
			template<typename, typename> typename Loader_,
			template<typename, typename> typename Fetcher_,
			typename DataType_,
			typename KeyType_
		>
		struct SystemLoader
		{
			using Domain = Domain_;
			template
			<
				typename From,
				typename To
			>
			using Loader = Loader_< From, To >;
			template
			<
				typename DataType,
				typename KeyType
			>
			using Fetcher = Fetcher_< DataType, KeyType >;
			using DataType = DataType_;
			using KeyType = KeyType_;

			using Self = SystemLoader< Domain_, Loader_, Fetcher_, DataType_, KeyType_ >;

			using EntityManager = typename Domain::EntityManager;
			using LoaderWrapper = std::function<
				void(EntityManager&, const DataType&)
			>;

			std::map< KeyType, LoaderWrapper > loaders;

			template<typename System>
			Self& add(const KeyType& key)
			{
				loaders[key] = [](EntityManager& em, const DataType& data)
				{
					System* system = new System();
					if(Loader< DataType, System >::load(data, *system))
						em.add_system(system);
					else
						delete system;
				};
				return *this;
			}

			void load(EntityManager& em, const DataType& data_set)
			{
				using Fetch = Fetcher< DataType, KeyType >;
				for(auto& loader : loaders)
				{
					const KeyType& key = loader.first;
					LoaderWrapper& fn = loader.second;

					DataType data;
					if(Fetch::fetch(data_set, key, data))
						fn(em, data);
				}
			}

		};


		template
		<
			typename Domain_
		>
		struct JsonSceneLoader
		{
			using Domain = Domain_;
			template<typename From, typename To>
			using Loader = Data::Loader<From, To>;

			using DataType = json11::Json;
			using KeyType = std::string;

			using Entity = typename Domain::Entity;
			using EntityManager = typename Domain::EntityManager;
			using EntityID = typename Domain::Entity::ID;

			using EntityLoader = LoaderDispatcher< Domain, Loader, ComponentDataFetch, DataType, KeyType >;
			using SystemLoader = SystemLoader< Domain, Loader, SystemDataFetch, DataType, KeyType >;

			EntityManager& em;
			EntityLoader entity_loader;
			SystemLoader system_loader;

			std::map< std::string, EntityID > named_entities;

			JsonSceneLoader(EntityManager& em)
			: em(em)
			{
				entity_loader
					.template add< Components::Position >("position")
					.template add< Components::Velocity >("velocity")
					.template add< Components::Shape >("shape")
					.template add< Components::Color >("color");

				// system_loaders["physics"] = [](EntityManager& em, const json11::Json& json)
				// {
				// 	auto system = new Systems::Physics();
				// 	em.add_system(system);
				// };
				// system_loaders["render"] = [](EntityManager& em, const json11::Json& json)
				// {
				// 	auto system = new Systems::Render();
				// 	em.add_system(system);
				// 	Loader< json11::Json, Systems::Render >::load(json, *system);
				// };

				system_loader
					.template add< Systems::Physics >("physics")
					.template add< Systems::Render >("render");
			}

			void load_children(const json11::Json& data, EntityID root = 0)
			{
				for(const auto& item : data.array_items())
				{
					if(item.is_object())
					{
						auto entity = em.create_entity(root);
						if(item["name"].is_string())
							named_entities[item["name"].string_value()] = entity.id;

						const auto& components = item["components"];
						entity_loader.load(entity, components);

						const auto& children = item["children"];
						if(children.is_array())
						{
							load_children(children, entity.id);
						}
					}
				}
			}

			void load_systems(const json11::Json& data)
			{
				for(const auto& item : data.array_items())
				{
					system_loader.load(em, item);

					// const std::string& type = item["type"].string_value();
					// const auto loader = system_loaders.find(type);
					// if(loader != system_loaders.end())
					// 	loader->second(em, item);
				}
			}

			void load_scene(const json11::Json& data, EntityID root = 0)
			{
				load_systems(data["systems"]);
				load_children(data["entities"], root);
			}

		};

	} // Data
} // SimpleGame
