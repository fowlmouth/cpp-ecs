#pragma once

#include <iostream>

#include "components/position.hpp"
#include "components/velocity.hpp"
#include "components/shape.hpp"
#include "components/color.hpp"
