// get json11 from here
//   git clone git@github.com:dropbox/json11.git
// compile
//   clang++ -std=c++1z test2.cpp json11/json11.cpp -o test2

#include "sceneloader.hpp"

#include <iostream>
#include <fstream>
#include <streambuf>
#include <filesystem>

namespace SimpleGame
{


	namespace Callbacks
	{
		struct StatefulCallback
		{
			std::vector< Domain::Entity::ID > ids;

			template<typename... Cs>
			void operator() (const Domain::Entity& entity, Cs& ...arg)
			{
				ids.push_back(entity.id);
			}
		};
	}


	struct App
	{
		Domain::EntityManager em;
		Systems::Render* render_system = nullptr;
		Domain::Entity scene;

		bool load_scene(const std::string& filename, std::string& error)
		{
			if(!std::filesystem::exists(filename))
			{
				error = "File does not exist: ";
				error += filename;
				return false;
			}

			std::ifstream file(filename);
			std::string test_json_string;
			file.seekg(0, std::ios::end);
			test_json_string.reserve(file.tellg());
			file.seekg(0, std::ios::beg);
			test_json_string.assign(
				std::istreambuf_iterator<char>(file),
				std::istreambuf_iterator<char>()
			);

			std::string err;
			json11::Json test_json = json11::Json::parse(test_json_string, err);

			if(err.size())
			{
				error = "Error parsing json: ";
				error += err;
				return false;
			}

			Data::JsonSceneLoader< Domain > scene_loader(em);
			scene_loader.load_scene(test_json);

			auto iter = scene_loader.named_entities.find("Scene");
			if(iter == scene_loader.named_entities.end())
			{
				error = "Missing entity named 'Scene'";
				return false;
			}
			auto scene_id = iter->second;

			scene = em.get(scene_id); //scene_loader.named_entities["Scene"]);

			for(auto system : em.systems)
			{
				render_system = dynamic_cast< Systems::Render* >(system);
				if(render_system)
					break;
			}

			if(!render_system)
			{
				render_system = new Systems::Render();
				em.add_system(render_system);

				render_system->window.create(sf::VideoMode(800,600), "scene");
			}

			return true;
		}

		void handleEvent(sf::Event& event)
		{
			switch(event.type)
			{
			case sf::Event::Closed:
				render_system->window.close();
				break;

			default:
				break;
			}
		}

		void run()
		{
			while(render_system->window.isOpen())
			{
				sf::Event event;
				while(render_system->window.pollEvent(event))
					handleEvent(event);

				render_system->render(scene);
			}
		}
	};


} // SimpleGame

int main(int argc, const char** argv)
{
	const char* filename = "demo-data.json";
	if(argc == 2) filename = argv[1];

	std::string error;
	SimpleGame::App app;

	if(! app.load_scene(filename, error))
	{
		std::cerr << error << std::endl;
		return 1;
	}

	app.run();


	return 0;
}
