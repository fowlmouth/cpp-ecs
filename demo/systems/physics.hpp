#pragma once

#include "../components/position.hpp"
#include "../components/velocity.hpp"

namespace SimpleGame {
	namespace Systems {

		struct Physics
		: Domain::System< Components::Position, Components::Velocity >
		{
			void update(float dt)
			{
				each([&](const Entity& entity, Components::Position& pos, Components::Velocity& vel){
					pos.x += vel.x * dt;
					pos.y += vel.y * dt;
				});
			}
		};

	} // Systems

	namespace Data {

		template<>
		struct Loader< json11::Json, Systems::Physics >
		{
			static bool load(const json11::Json& data, Systems::Physics& system)
			{
				return true;
			}
		};

	} // Data
} // SimpleGame
