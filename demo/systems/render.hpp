#pragma once

#include <SFML/Graphics.hpp>

#include "../components/position.hpp"
#include "../components/shape.hpp"

namespace SimpleGame {
	namespace Systems {

		struct Render
		: Domain::System< Components::Position, Components::Shape >
		{
			sf::RenderWindow window;

			struct RenderVisitor
			{
				Render& renderer;

				RenderVisitor(Render& r)
				: renderer(r)
				{
				}

				void operator() (Domain::Entity entity) const
				{
					auto shape = entity.get< Components::Shape >();
					auto position = entity.get< Components::Position >();
					auto color = entity.get< Components::Color >();
					if(shape.is_valid() && position.is_valid())
						switch(shape->shape)
						{
							case Components::Shape::Circle:
							{
								sf::CircleShape cs;
								cs.setRadius(shape->radius);
								// cs.setOrigin(shape->radius/2, shape->radius/2);
								cs.setOrigin(shape->origin);
								cs.setPosition(position->x, position->y);
								if(color.is_valid())
									cs.setFillColor(*color);

								renderer.window.draw(cs);
								break;
							}
							case Components::Shape::Poly:
							{
								sf::ConvexShape cs(4);
								for(int i = 0; i < shape->points.size(); ++i)
									cs.setPoint(i, shape->points[i]);
								cs.setOrigin(shape->origin);
								cs.setPosition(position->x, position->y);
								if(color.is_valid())
									cs.setFillColor(*color);

								renderer.window.draw(cs);
								break;
							}
							default:
								break;
						}

					entity.each_child(*this);
				}
			};

			void render(Domain::Entity scene)
			{
				window.clear(sf::Color::Black);

				// RenderVisitor visitor(*this);
				scene.each_child(RenderVisitor(*this));

				window.display();
			}
		};

	} // Systems

	namespace Data {

		template<>
		struct Loader< json11::Json, Systems::Render >
		{
			static bool load(const json11::Json& json, Systems::Render& system)
			{
				int width = 800, height = 600, fps = 60;
				std::string caption = "scene";

				if(json["width"].is_number())
					width = json["width"].int_value();
				if(json["height"].is_number())
					height = json["height"].int_value();
				if(json["caption"].is_string())
					caption = json["caption"].string_value();
				if(json["fps"].is_number())
					fps = json["fps"].int_value();

				system.window.create(sf::VideoMode(width, height), caption);
				system.window.setFramerateLimit(fps);
				return true;
			}
		};

	} // Data
} // SimpleGame
