#!/bin/sh

CPP=clang++
# CPPVERBOSE="-v"

#macos specific llvm 8 and openal paths
LDFLAGS="-L/usr/local/opt/llvm/lib -Wl,-rpath,/usr/local/opt/llvm/lib"
CFLAGS="-I/usr/local/opt/llvm/include"
CPATH="/usr/local/opt/llvm/bin/"


DEBUG_CFLAGS="-g -O0"
RELEASE_CFLAGS="-O3"


CFLAGS="$CFLAGS $DEBUG_CFLAGS -std=c++1z -I../src"
LDFLAGS="$LDFLAGS -lc++fs"


CFILES="main.cpp json11/json11.cpp"
OUTPUT=ecs-demo

export PKG_CONFIG_PATH=/usr/local/opt/openal-soft/lib/pkgconfig

$CPATH$CPP $CPPVERBOSE $CFLAGS \
	`pkg-config --cflags sfml-all` \
	$CFILES -o $OUTPUT $LDFLAGS \
	`pkg-config --libs sfml-all`
