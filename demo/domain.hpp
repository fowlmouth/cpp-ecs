#pragma once
#include "../include/fowl/entity.h"

namespace SimpleGame
{

	struct simple_game_types;
	using Domain = fowl::entity::Domain< simple_game_types >;

} // SimpleGame
